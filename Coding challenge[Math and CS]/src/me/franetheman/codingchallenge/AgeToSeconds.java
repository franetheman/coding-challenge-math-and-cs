package me.franetheman.codingchallenge;

public class AgeToSeconds {

	public AgeToSeconds() {

	}

	public long convertToSeconds(int years, int months, int weeks, int days, int hours, int minutes) {
		return years * EnumTime.YEAR.inSeconds + months * EnumTime.MONTH.inSeconds + weeks * EnumTime.WEEK.inSeconds
				+ days * EnumTime.DAY.inSeconds + hours * EnumTime.HOUR.inSeconds + EnumTime.MINUTE.inSeconds;
	}

	public enum EnumTime {

		YEAR(31536000), MONTH(2592000), WEEK(604800), DAY(86400), HOUR(3600), MINUTE(60);

		private long inSeconds;

		private EnumTime(long inSeconds) {
			this.inSeconds = inSeconds;
		}

		public long inSeconds() {
			return inSeconds;
		}

	}
}
