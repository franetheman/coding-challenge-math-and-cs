package me.franetheman.codingchallenge;

import java.util.Base64;

public class EncryptAndDecrypt {

	//TODO
	public EncryptAndDecrypt() {

	}

	public String encrypt(String entry) {
		return Base64.getEncoder().encodeToString(entry.getBytes());
	}
	
	public String decrypt(String encodedEntry){
		return Base64.getDecoder().decode(encodedEntry).toString();
	}
}
