package me.franetheman.codingchallenge;

import java.math.BigInteger;

public class Factorial {

	public Factorial(){
		
	}
	
	public  BigInteger fact(long n) {
	    BigInteger result = BigInteger.ONE;
	    for (long i = 2; i <= n; i++)
	        result = result.multiply(BigInteger.valueOf(i));
	    return result;
	}
}
