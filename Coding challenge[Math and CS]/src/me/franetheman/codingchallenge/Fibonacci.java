package me.franetheman.codingchallenge;

public class Fibonacci {

	public Fibonacci(){
		
	}
	
	public void fibonacciSequence(int fibonnaciCount){
        int[] feb = new int[fibonnaciCount];
        feb[0] = 0;
        feb[1] = 1;
        for(int i=2; i < fibonnaciCount; i++){
            feb[i] = feb[i-1] + feb[i-2];
        }

        for(int i=0; i< fibonnaciCount; i++){
                System.out.print(feb[i] + " ");
        }
	}
}
