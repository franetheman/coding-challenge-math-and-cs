package me.franetheman.codingchallenge;

public class BMICalulator {

	public BMICalulator(){
		
	}
	
	public strictfp double calculateBMI(double heightMeters, double weightKilos){
		return weightKilos/(heightMeters * heightMeters);
	}
}
