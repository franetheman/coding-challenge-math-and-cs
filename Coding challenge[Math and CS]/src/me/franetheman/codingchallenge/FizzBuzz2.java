package me.franetheman.codingchallenge;

import static java.lang.System.*;

public class FizzBuzz2{

	public FizzBuzz2() {
		process();
	}
	
	public void process(){
		
		
		for(int i=1;i<100;i++,out.println(i%3==0||i%5==0?((i%3)==0?"fizz":"")+((i%5)==0?"buzz":""):i));
		
		
	}
	
}
